package com.example.marketmaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BfamInterviewExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BfamInterviewExerciseApplication.class, args);
	}

}
