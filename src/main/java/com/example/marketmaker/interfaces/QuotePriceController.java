package com.example.marketmaker.interfaces;

import com.example.marketmaker.domain.CalculateQuotePrice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/quote-price")
public class QuotePriceController {

    @GetMapping("/get-price")
    public double getQuotePrice(@RequestParam(value = "securityId") int securityId,
                                @RequestParam(value = "buy") boolean buy,
                                @RequestParam(value = "quantity") int quantity) {
        CalculateQuotePrice calculateQuotePrice = new CalculateQuotePrice();
        return calculateQuotePrice.calculateQuotePrice(securityId, buy, quantity);
    }
}
