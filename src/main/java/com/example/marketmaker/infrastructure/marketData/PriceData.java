package com.example.marketmaker.infrastructure.marketData;

import lombok.Data;

@Data
public class PriceData {
    int securityId;
    double quotePrice;
    String currency;
    boolean isBuy;

    public PriceData(int securityId, double quotePrice, String currency, boolean isBuy) {
        this.securityId = securityId;
        this.quotePrice = quotePrice;
        this.currency = currency;
        this.isBuy = isBuy;
    }
}
