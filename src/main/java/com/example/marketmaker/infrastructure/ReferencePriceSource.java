package com.example.marketmaker.infrastructure;

/**
 * Source for reference prices.
 */
public interface ReferencePriceSource {
    double get(int securityId);
}
