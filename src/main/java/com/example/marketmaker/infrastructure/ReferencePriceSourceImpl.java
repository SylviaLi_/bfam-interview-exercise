package com.example.marketmaker.infrastructure;

import com.example.marketmaker.infrastructure.marketData.PriceData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ReferencePriceSourceImpl implements ReferencePriceSource {
    @Override
    public double get(int securityId) {
        List<PriceData> priceDataList = getMarketDataList();
        Optional<PriceData> data = priceDataList.stream().filter(priceData -> priceData.getSecurityId() == securityId).findFirst();

        return data.map(PriceData::getQuotePrice).orElse(0.0);
    }

    private List<PriceData> getMarketDataList() {
        List<PriceData> priceDataList = new ArrayList<>();

        JSONParser parser = new JSONParser();
        try (FileReader reader = new FileReader("src/main/java/com/example/marketmaker/data/PriceData.json")) {
            Object obj = parser.parse(reader);
            JSONArray objectList = (JSONArray) obj;

            objectList.forEach(data -> priceDataList.add(parseEmployeeObject((JSONObject) data)));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return priceDataList;
    }

    private PriceData parseEmployeeObject(JSONObject jsonObject) {
        JSONObject employeeObject = (JSONObject) jsonObject.get("marketData");

        return new PriceData(
                (int) (long) employeeObject.get("securityId"),
                (double) (long) employeeObject.get("quotePrice"),
                (String) employeeObject.get("currency"),
                (boolean) employeeObject.get("isBuy")
        );
    }
}
