package com.example.marketmaker.domain;

public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        return buy ? - quantity * referencePrice: quantity * referencePrice;
    }
}
