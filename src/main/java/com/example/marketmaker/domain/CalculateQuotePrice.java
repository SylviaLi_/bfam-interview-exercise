package com.example.marketmaker.domain;

import com.example.marketmaker.infrastructure.ReferencePriceSource;

public class CalculateQuotePrice {
    ReferencePriceSource referencePriceSource;
    QuoteCalculationEngine quoteCalculationEngine;

    public double calculateQuotePrice(int securityId, boolean buy, int quantity){
        double price = referencePriceSource.get(securityId);
        return quoteCalculationEngine.calculateQuotePrice(securityId, price, buy, quantity);
    }
}
