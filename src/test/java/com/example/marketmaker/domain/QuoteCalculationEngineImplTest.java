package com.example.marketmaker.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class QuoteCalculationEngineImplTest {
    QuoteCalculationEngineImpl quoteCalculationEngine = new QuoteCalculationEngineImpl();

    @Test
    public void testCalculateQuotePriceForBuy(){
        int securityId = 10;
        double actual = quoteCalculationEngine.calculateQuotePrice(securityId, 100, true, 200);
        double expected = -20000.0;
        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateQuotePriceForSell(){
        int securityId = 10;
        double actual = quoteCalculationEngine.calculateQuotePrice(securityId, 100, false, 200);
        double expected = 20000.0;
        assertEquals(expected, actual);
    }
}