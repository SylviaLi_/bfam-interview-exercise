package com.example.marketmaker.infrastructure;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class ReferencePriceSourceImplTest {
    ReferencePriceSourceImpl referencePriceSource = new ReferencePriceSourceImpl();

    @Test
    public void testGetPrice(){
        int securityId = 1;
        assertEquals(1, referencePriceSource.get(securityId));
    }
}